//
//  InterfaceController.swift
//  watchActivity WatchKit Extension
//
//  Created by Vishwanath Keerthi on 2019-06-29.
//  Copyright © 2019 Vishwanath Keerthi. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController,WCSessionDelegate {
    let session = WCSession.default
    var watchCounter = 0
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    

    @IBOutlet weak var OutputLabel: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        initWCsession()
    }
    
    
    func initWCsession() {
        session.delegate = self
        session.activate()
        
    }
    
    
    //recieve data from phone
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        let msg = message["CounterValueFromPhone"] as! Int
        OutputLabel.setText("Counter : \(msg)")
    }
    
    
    
    
    
    
    
    
    
//    override func didDeactivate() {
//        // This method is called when watch view controller is no longer visible
//        super.didDeactivate()
//    }

    
    
    
    //send data to phone
    
    @IBAction func ButtonPressed() {
        
        let msg = ["CounterValueFromWatch": watchCounter+=1]
        session.sendMessage(msg, replyHandler: { (replay) in
            
        }) { (error) in
            
        }
    }
}
