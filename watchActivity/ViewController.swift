//
//  ViewController.swift
//  watchActivity
//
//  Created by Vishwanath Keerthi on 2019-06-29.
//  Copyright © 2019 Vishwanath Keerthi. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate{
    
    let session = WCSession.default
    var phoneCounter = 0
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    @IBOutlet weak var OutputLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        initWCsession()
        // Do any additional setup after loading the view.
    }

    func initWCsession() {
        session.delegate = self
        session.activate()
        
    }
    
    //recieve data from watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
              let msg = message["CounterValueFromWatch"] as! Int
              OutputLabel.text = ("Counter : \(msg)")
           }
    
    
    
    
    
    
    
    //send data to watch
    
    @IBAction func ButtonPressed(_ sender: Any) {
        
        let msg = ["CounterValueFromPhone": phoneCounter+=1]
        session.sendMessage(msg, replyHandler: { (replay) in
            
        }) { (error) in
            
        }
    }
    
}

